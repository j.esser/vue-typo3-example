<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'JE.Immovue',
            'Listing',
            'Immo Listing'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('immovue', 'Configuration/TypoScript', 'Immovue');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_immovue_domain_model_property', 'EXT:immovue/Resources/Private/Language/locallang_csh_tx_immovue_domain_model_property.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_immovue_domain_model_property');

    }
);

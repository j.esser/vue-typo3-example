<?php


namespace JE\Immovue\Controller;


class ApiController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * @var \TYPO3\CMS\Extbase\Mvc\View\JsonView
     */
    protected $view;

    /**
     * @var string
     */
    protected $defaultViewObjectName = \TYPO3\CMS\Extbase\Mvc\View\JsonView::class;

    /**
     * @var \JE\Immovue\Domain\Repository\PropertyRepository
     * @inject
     */
    protected $propertyRepository;

    /**
     * action show
     *
     * @param \JE\Immovue\Domain\Model\Property $property
     * @return void
     */
    public function showAction(\JE\Immovue\Domain\Model\Property $property)
    {
        $this->view->setVariablesToRender(['property']);
        $this->view->assign('property', $property);
    }

    public function listAction() {
        $this->view->setVariablesToRender(['properties']);
        $properties = $this->propertyRepository->findAll();
        $this->view->assign('properties', $properties);
    }

    /**
     * @param $data
     */
    public function filterAction() {
        error_log(print_r($this->request->getArguments(), true));
        $this->view->setVariablesToRender(['properties']);
        $this->view->assign('properties', $this->propertyRepository->findFiltered($this->request->getArgument('filter')));
    }
}
<?php
namespace JE\Immovue\Controller;


use JE\Immovue\Domain\Repository\PropertyRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext;
use TYPO3\CMS\Extbase\Mvc\View\JsonView;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/***
 *
 * This file is part of the "Immovue" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Jonas Esser <info@jonas-esser.de>
 *
 ***/
/**
 * PropertyController
 */
class PropertyController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{


    /**
     * @var \JE\Immovue\Domain\Repository\PropertyRepository
     * @inject
     */
    protected $propertyRepository;
    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $properties = $this->propertyRepository->findAll();

        $jsonView = $this->objectManager->get(JsonView::class);
        $jsonView->setControllerContext($this->objectManager->get(ControllerContext::class));
        $jsonView->setVariablesToRender(['properties']);
        $jsonView->assign('properties', $properties);
        $json = $jsonView->render();
        $this->view->assign('properties', $properties);
        $this->view->assign('json', $json);
    }

    /**
     * action show
     * 
     * @param \JE\Immovue\Domain\Model\Property $property
     * @return void
     */
    public function showAction(\JE\Immovue\Domain\Model\Property $property)
    {
        $this->view->assign('property', $property);
    }
}

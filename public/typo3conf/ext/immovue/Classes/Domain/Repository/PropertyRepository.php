<?php


namespace JE\Immovue\Domain\Repository;


class PropertyRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    public function findFiltered($filter) {
        $query = $this->createQuery();
        $constraints = [];
        if ($filter['maxPrice']) {
            error_log(print_r("in price", true));
            
            $constraints[] = $query->lessThanOrEqual('price', $filter['maxPrice']);
        }
        if ($filter['search']) {
            $constraints[] = $query->logicalOr(
                $query->like('title', '%'.$filter['search'].'%'),
                $query->like('description', '%'.$filter['search'].'%')
            );
        }
        
        if (count($constraints) > 0) {

            $query->matching(
                $query->logicalAnd(
                    $constraints
                )
            );
        }
        $queryParser = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser::class);
        error_log(print_r($queryParser->convertQueryToDoctrineQueryBuilder($query)->getSQL(), true));


        return $query->execute();
    }
}
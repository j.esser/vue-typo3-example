<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'JE.Immovue',
            'Listing',
            [
                'Property' => 'list, show',
            ],
            // non-cacheable actions
            [
                'Property' => '',
            ]
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'JE.Immovue',
            'Api',
            [
                'Api' => 'list, show, filter'
            ],
            // non-cacheable actions
            [
                'Api' => 'list, show, filter'

            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    listing {
                        iconIdentifier = immovue-plugin-listing
                        title = LLL:EXT:immovue/Resources/Private/Language/locallang_db.xlf:tx_immovue_listing.name
                        description = LLL:EXT:immovue/Resources/Private/Language/locallang_db.xlf:tx_immovue_listing.description
                        tt_content_defValues {
                            CType = list
                            list_type = immovue_listing
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'immovue-plugin-listing',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:immovue/Resources/Public/Icons/user_plugin_listing.svg']
			);
		
    }
);

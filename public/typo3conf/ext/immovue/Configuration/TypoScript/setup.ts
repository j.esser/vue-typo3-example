
plugin.tx_immovue_listing {
    view {
        templateRootPaths.0 = EXT:{extension.extensionKey}/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_immovue_listing.view.templateRootPath}
        partialRootPaths.0 = EXT:immovue/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_immovue_listing.view.partialRootPath}
        layoutRootPaths.0 = EXT:immovue/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_immovue_listing.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_immovue_listing.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}
plugin.tx_immovue_api {
    persistence {
        storagePid = {$plugin.tx_immovue_listing.persistence.storagePid}
        #recursive = 1
    }
    features {
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 0
    }
}

immovue_api = PAGE
immovue_api {
    config {
        disableAllHeaderCode = 1
        debug = 0
        no_cache = 1
        additionalHeaders {
            10 {
                header = Content-Type: application/json
                replace = 1
            }
        }
    }
    typeNum = 1557996244
    10 < tt_content.list.20.immovue_api
}

page.includeCSS {
    immovueApp = EXT:immovue/Resources/Public/css/app.css
    immovueApp.external = 1
    immovueChunkVendors = EXT:immovue/Resources/Public/css/chunk-vendors.css
    immovueChunkVendors.external = 1
}
[applicationContext = Development]
page.includeJSFooterlibs {
    immovueDevApp = EXT:immovue/Resources/Public/app.js
    immovueDevApp.external = 1
}
[END]
[applicationContext = Production]
page.includeJSFooterlibs {
    immovueApp = EXT:immovue/Resources/Public/js/app.js
    immovueApp.external = 1
    immovueChunkVendors = EXT:immovue/Resources/Public/js/chunk-vendors.js
    immovueChunkVendors.external = 1
}
[END]
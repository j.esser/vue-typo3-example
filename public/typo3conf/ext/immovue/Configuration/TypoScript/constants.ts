
plugin.tx_immovue_listing {
    view {
        # cat=plugin.tx_immovue_listing/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:immovue/Resources/Private/Templates/
        # cat=plugin.tx_immovue_listing/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:immovue/Resources/Private/Partials/
        # cat=plugin.tx_immovue_listing/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:immovue/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_immovue_listing//a; type=string; label=Default storage PID
        storagePid =
    }
}

#
# Table structure for table 'tx_immovue_domain_model_property'
#
CREATE TABLE tx_immovue_domain_model_property (

	title varchar(255) DEFAULT '' NOT NULL,
	description text,
	price int DEFAULT 0 NOT NULL,

);
